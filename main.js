// const column1 = document.getElementById('column-1');
// const column2 = document.getElementById('column-2');
let startingPiece;
//This will move a piece by appending it to element clicked
//set up via onclick="movePiece(this) -- 'this' referring to element being clicked"
function movePiece(element) {
    if (!startingPiece) { startingPiece = element.firstElementChild; } else {
        startingPiece.remove();
        element.appendChild(startingPiece);
        startingPiece = null;
    }
}

function generateCircle(element) {
    let newCircle = document.createElement('div');
    newCircle.style.border = "1px solid black";
    newCircle.style.borderRadius = "50%";
    newCircle.style.height = "83px";
    newCircle.style.width = "100px";
    newCircle.style.background = 'red';
    element.appendChild(newCircle);
    newCircle.addEventListener('click', function() {
        newCircle.style.background = 'black';
    })
}
//Color of circles should alternate when clicked on
// const redPieces = document.querySelectorAll('.pieces_red');
// const blackPieces = document.querySelectorAll('.pieces_black');

// redPieces.addEventListener('click', function() {
//     redPieces.style.background = 'black';
// })

// blackPieces.addEventListener('click', function() {
//     blackPieces.style.background = 'red';
// })

//Thinking how to alternate between circles
//sets start at undefined; 

let start = false;

const player1 = function(element) {
    let newDiv = document.createElement('div')
    newDiv.style.width = "100px";
    newDiv.style.height = "83px";
    newDiv.style.border = "1px solid black";
    newDiv.style.borderRadius = '50%';
    newDiv.style.background = 'red';
    element.appendChild(newDiv);
    console.log(element)
}

const player2 = function(element) {
    var newDiv = document.createElement('div');
    newDiv.style.width = "100px";
    newDiv.style.height = "83px";
    newDiv.style.border = "1px solid black";
    newDiv.style.borderRadius = '50%';
    newDiv.style.background = 'black';
    element.appendChild(newDiv);
    start = false;
    console.log(element)
}

function makeMove(element) {
    if (!start) { //this is saying, if start is NOT true --which it won't be after we reset
        start = true;
        player1(element);

    } else {
        start = false;
        player2(element);
    }
}